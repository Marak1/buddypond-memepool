# Buddy Pond MemePool

This repository contains a collection of memes used at Buddypond.com

Feel free to make a PR to add your own.

## See Mnemosyne Live in action at Buddy Pond!!!

[https://buddypond.com](https://buddypond.com)

# Mnemosyne

*Mnemosyne is the goddess of memory and the mother of the nine Muses.*

![Mnemosyne](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Mosaïque_murale_Mnémosyne.jpg/440px-Mosaïque_murale_Mnémosyne.jpg)

`Mnemosyne` is used to query the Memepool and return the most excellant results based on: Levenstein Distance, Jaro Winkler Values, plus  some custom search algos.

You may call `Mnemosyne.query(searchString)` and get back an array of Meme results which will have relative context based on your query.

## CLI ( Command Line Interface )

`node ./bin/Mnemosyne.js cool`

```
{
  author: 'Mnemosyne',
  type: 'image',
  filename: 'cool-sand-job-brother-dune-surf.gif',
  title: 'cool sand job brother dune surf',
  levenshtein: 27,
  winkler: '0.826',
  set: [
    {
      filename: 'cool-internet-job-bro.gif',
      levenshtein: 17,
      winkler: 0.8380952380952381
    },
    {
      filename: 'cool-sand-job-brother-dune-surf.gif',
      levenshtein: 27,
      winkler: 0.8258064516129032
    },
    {
      filename: 'sonic-snowboard-cool-santa.gif',
      levenshtein: 22,
      winkler: 0.4583333333333333
    }
  ]
}
```

## Examples

See `./examples` for code examples

`node examples/query-memepool.js`