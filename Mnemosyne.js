let Mnemosyne = {};
let path = require('path');
let fs = require('fs');

const levenshtein = require('./vendor/levenshtein');
const winkler = require('./vendor/winkler');

let memePath = path.resolve(__dirname + "/memes/");

let clientPath = path.resolve(__dirname + "/memes.js");

Mnemosyne.load = function loadMemes () {
  let files = fs.readdirSync(memePath);

  // TODO: use .gitignore file instead of hard-coded array here
  let ignored = ['.DS_Store']
  files = files.filter(function(f){
    if (ignored.indexOf(f) === -1) {
      return true;
    }
    return false;
  });
  // write these files to the public buddypond-client path
  let str = JSON.stringify(files, true, 2);
  str = 'let Memepool = ' + str + ';';
  fs.writeFileSync(clientPath, str);
  return files;
}

// console.log('memePath', memePath);
Mnemosyne.query = function (str) {

  // let str = 'future is now old man';
  let results = [];

  let searchSet = [];

  let directMatches = 0;

  // lots of memes to parse through
  // first, let's narrow down the list for exact word matches
  // most likely, we should find a few. use this list for further processing
  // if no results were returned, use the entire lisf fur further processing
  let words = str.split(' ');
  Mnemosyne.keys.forEach(function(m){
    let found = false;
    /*
      checks for " word-", "-word-", and "-word" formats
    */
    words.forEach(function(word){
      if (
        (
          m.search(word + '-') !== -1 ||
          m.search('-' + word + '-') !== -1 ||
          m.search(word + '-') !== -1
        )
        && searchSet.indexOf(m) === -1) {
        searchSet.push(m);
      }
    });
  });

  directMatches = searchSet.length;

  // if no direct matches were found, use the entire dataset instead
  if (directMatches === 0) {
    searchSet = Mnemosyne.keys;
  }
  searchSet.forEach(function(m){
    let filename = m.split('.')[0];
    let lDistance = levenshtein(str, filename);
    let wDistance = winkler.distance(str, filename);
    results.push({ filename: m, levenshtein: lDistance, winkler: wDistance });
  });

  // if there is a larger search set, filter down with Jaro Winkler scores
  if (results.length > 10) {
    results = results.filter(function(a){
      if (a.winkler < 0.5555) {
        return false
      }
      return true;
    });
  }

  results = results.sort(function(a, b){
    if (a.winkler > b.winkler) {
      return 1;
    }
    if (b.winkler > a.winkler) {
      return -1;
    }
    return 0;

  });

  results.reverse();

  // choose a random result from the search set
  let randomImage;

  // no results were found, this is unlikely to happen
  // most likely an empty search query was sent
  if (results.length === 0) {
    let filename = searchSet[Math.floor(Math.random() * searchSet.length)];
    randomImage = { filename: filename, levenshtein: 0, winkler: 0 };
  } else {
    // we have results, use them
    randomImage = results[Math.floor(Math.random() * results.length)];
  }

  // this shouldn't happen now
  if (!randomImage) {
    return false;
  }

  // trim results down to 20 in case a large set has been searched
  if (results.length > 20) {
    results = results.splice(0, 20);
  }

  let imageTitle = randomImage.filename.replace(/-/g, ' ');
  imageTitle = imageTitle.split('.')[0];
  let card = {
    'author': 'Mnemosyne',
    'type': 'image',
    'filename': randomImage.filename,
    'title': imageTitle,
    'levenshtein': randomImage.levenshtein,
    'winkler': randomImage.winkler.toFixed(3),
    'set': results
  };
  // console.log('returning', card)
  return card;
}

Mnemosyne.keys = Mnemosyne.load();

//console.log(memes.keys)

module.exports = Mnemosyne;